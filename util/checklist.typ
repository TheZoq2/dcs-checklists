#let i(item, condition, style: none) = {
  box([
    #let color = if style == "event" {
      purple
    } else if style == "note" {
      gray
    } else if style == "verify" {
      blue
    } else if style == none{
      black
    } else {
      red
    }
    #set text(fill: color)

    #item
    #box(width: 1fr, line(length: 100%, stroke: 1pt + gray.lighten(40%)))
    #condition
  ])
}

#let from_dsl(dsl) = {
  let lines = dsl.split("\n")

  let result = []

  for line in lines {
    let style_regex = regex(">\w+")

    let style = line.find(style_regex)
    let style = if style != none {
      style.replace(">", "").trim()
    } else {
      style
    }

    let line_raw = line.trim().replace(style_regex, "")

    let pipe_split = line_raw.split("|")
    if pipe_split.len() == 2 {
      let (item, condition) = pipe_split

      result = result + i(item.trim(), condition.trim(), style: style)
    }
  }
  result
}
