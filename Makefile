CHKS=$(wildcard *.chk)
TYPSTS=$(wildcard *.typ)
PDFS=$(patsubst %.chk,build/%.pdf,${CHKS}) $(patsubst %.typ,build/%.pdf,${TYPSTS})
IMGS=$(patsubst %.chk,build/%,${CHKS})
INSTALL=$(patsubst %.chk,INSTALL/%,${CHKS})
TEMPLATE=template.tex

KNEEBOARD_DIR="/home/frans/.local/share/Steam/steamapps/compatdata/223750/pfx/drive_c/users/steamuser/Saved\ Games/DCS/Kneeboard"
STANDALONE_KNEEBOARD_DIR="/mnt/ssd1/Games/dcs-world/drive_c/users/frans/Saved\ Games/DCS.openbeta/Kneeboard"


install: ${INSTALL}

all: ${IMGS}

INSTALL/%: build/%
	mkdir -p "${KNEEBOARD_DIR}/${@F}"
	mkdir -p "${STANDALONE_KNEEBOARD_DIR}/${@F}"
	cp $(wildcard ../kneeboards/**/*.png) $(wildcard ../kneeboards/**/*.jpg) $(wildcard build/${@F}*.png) "${KNEEBOARD_DIR}/${@F}"
	cp $(wildcard ../kneeboards/**/*.png) $(wildcard ../kneeboards/**/*.jpg) $(wildcard build/${@F}*.png) "${STANDALONE_KNEEBOARD_DIR}/${@F}"


build/%.pdf: %.typ
	typst compile $< $@

build/%.tex: %.chk build_checklist.hs template.tex Makefile
	@echo -e "[\033[0;34mchk\033[0m] building tex file"
	@mkdir -p $(@D)
	@./build_checklist.hs ${TEMPLATE} $< $@

# build/Mi-24.pdf: mi24_flying_artillery.tex

build/%.pdf: build/%.tex *.tex Makefile
	@echo -e "[\033[0;34mpdflatex\033[0m] building pdf"
	@mkdir -p $(@D)
	@echo $@
	@pdflatex -halt-on-error -output-directory $(@D) $<

build/%: build/%.pdf Makefile
	@echo -e "[\033[0;34mpdftoppm\033[0m] building pngs"
	@mkdir -p $(@D)
	@pdftoppm $< $(@D)/${@F} -png
	@touch $@



clean:
	rm -rf build

.PRECIOUS: build/%.tex build/%.pdf
