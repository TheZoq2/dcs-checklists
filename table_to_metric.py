table = """5575 & 5625 & 5700 & 5750 & 5825 & 5875
 6750 & 6800 & 6875 & 6950 & 7025 & 7100
 --   & --   & 7350 & 7400 & 7475 & 7550
 5675 & 5725 & 5800 & 5850 & 5925 & 5975
 6800 & 6850 & 6925 & 7000 & 7075 & 7150
 --   & --   & 7400 & 7450 & 7525 & 7600
 5925 & 5975 & 6050 & 6100 & 6175 & 6225
 6950 & 7000 & 7075 & 7150 & 7225 & 7300
 --   & --   & 7550 & 7600 & 7675 & 7750
 6425 & 6475 & 6550 & 6600 & 6675 & 6725
 7150 & 7200 & 7275 & 7350 & 7425 & 7450
 --   & --   & 7650 & 7700 & 7775 & 7850
"""

for line in table.splitlines():
    for column in line.split("&"):
        stripped = column.strip()
        val = "--"
        if stripped != "--":
            val = round(int(stripped) * 5.399568 * 10**-4, 3)

        color = "black"
        if val == "--":
            color = "black"
        elif val < 3.5:
            color = "blue"
        elif val < 4.0:
            color = "teal"
        elif val < 4.5:
            color = "purple"

        print(f"&\\color{{{color}}}{{{val}}}", end="")
    print("&")
