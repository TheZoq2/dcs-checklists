#!/bin/stack
{- stack script
 --resolver lts-15.9
 --install-ghc
 --package "text"
 --package "string-interpolate"
 --package "haskell-stack-trace-plugin"
 --ghc-options -Wall
 --ghc-options -fplugin=StackTrace.Plugin
-}
{-# language OverloadedStrings #-}
{-# language QuasiQuotes #-}
{-# language BinaryLiterals #-}


import Control.Monad
import Data.String.Interpolate (i)
import qualified Data.List as L
import qualified Data.Text as T
import qualified Data.Text.IO as IO
import System.Environment (getArgs)
import System.Exit (die)


data Chunk
    = Empty
    | Title T.Text
    | Heading T.Text
    | Subheading T.Text
    | Table [(T.Text, T.Text)]
    -- Raw latex
    | Raw T.Text
    | NewPage


parseRows :: [T.Text] -> Maybe ([(T.Text, T.Text)], [T.Text])
parseRows [] = Nothing
parseRows (line:rest) =
    case T.splitOn "|" line of
        -- This line was a row, check if there are more
        [action, desired] ->
            case parseRows rest of
                Just (more, remain) -> Just (((action, desired):more), remain)
                Nothing -> Just ([(action, desired)], rest)
        _ -> Nothing


firstMatchingPrefix :: [(T.Text, T.Text -> a)] -> T.Text -> Maybe a
firstMatchingPrefix [] _ =
    Nothing
firstMatchingPrefix ((prefix, result):rest) text =
    case T.stripPrefix prefix text of
        Just postfix -> Just (result postfix)
        Nothing -> firstMatchingPrefix rest text

parseHeading :: [T.Text] -> Maybe (Chunk, [T.Text])
parseHeading (line:rest) =
    fmap (\match -> (match, rest))
        $ firstMatchingPrefix
            [("###", Subheading), ("##", Heading), ("#", Title)]
            line
parseHeading _ = Nothing

parseEmptyLine :: [T.Text] -> Maybe (Chunk, [T.Text])
parseEmptyLine ("":rest) = Just (Empty, rest)
parseEmptyLine _ = Nothing

parseNewPage :: [T.Text] -> Maybe (Chunk, [T.Text])
parseNewPage ("\\newpage":rest) = Just (NewPage, rest)
parseNewPage _ = Nothing

parseRaw :: [T.Text] -> Maybe (Chunk, [T.Text])
parseRaw (line:rest) =
    fmap (\match -> (match, rest))
        $ firstMatchingPrefix
            [("\\raw", Raw)]
            line
parseRaw _ = Nothing




parser
    :: (a -> Chunk)
    -> ([T.Text] -> Maybe (a, [T.Text]))
    -> ([T.Text] -> Maybe (Chunk, [T.Text]))
parser chunk inner =
    let
        converter = (\(x, rest) -> (chunk x, rest))
    in
    (\input -> fmap converter $ inner input)


-- Parse a checklist file into chunks
parseChecklist :: [T.Text] -> [Chunk]
parseChecklist [] = []
parseChecklist content =
    let
        matches =
            [ parser Table parseRows content
            , parser id parseHeading content
            , parser id parseNewPage content
            , parser id parseEmptyLine content
            , parser id parseRaw content
            ]

    in
        case msum matches of
            Just (chunk, rest) -> (chunk:(parseChecklist rest))
            Nothing -> error [i|Failed to parse starting at line #{head content}|]


colored :: T.Text -> T.Text -> T.Text
colored color text =
    [i|\\noindent{\\color{#{color}}#{text}}|]

beautifyAction :: T.Text -> T.Text
beautifyAction raw =
    let
        prefixes =
            [(">weapon", colored "red")
            ,(">note", colored "gray")
            ,(">warn", colored "orange")
            ,(">sub", colored "blue")
            ,(">event", colored "violet")
            ,(">verify", colored "olive")
            ]
    in
        maybe raw (id) $ firstMatchingPrefix prefixes raw


rowToTex :: (T.Text, T.Text) -> T.Text
rowToTex (action, state) =
    [i|#{beautifyAction action} & #{state}|]

chunkToTex :: Chunk -> [T.Text]
chunkToTex chunk =
    case chunk of
        Empty -> []
        Title name -> [[i|\\section*{#{name}}|]]
        Heading name ->
            [ [i|\\subsection*{#{name}}|] ]
        Subheading name ->
            [ [i|\\subsubsection*{#{name}}|] ]
        Table rows ->
            [ "\\vspace{-0.2cm}"
            , "\\begin{table}[H]"
            , "    \\begin{tabularx}{\\columnwidth}{@{} X r @{}}"
            ]
            ++ (L.intersperse "\\\\" $ fmap rowToTex rows)
            ++
            [ "    \\end{tabularx}"
            , "\\end{table}"
            , "\\vspace{-0.3cm}"
            ]
        NewPage ->
            [ "\\newpage" ]
        Raw text ->
            [ text ]

convertToTex :: T.Text -> [T.Text] -> T.Text
convertToTex template checklist =
    let
        inner =
            T.unlines
                $foldr (++) []
                $ fmap chunkToTex
                $ parseChecklist checklist
    in
    T.replace "%CONTENT" inner template



parseArgs :: [String] -> IO ()
parseArgs [templateFile,inFile,outFile] = do
    template <- IO.readFile templateFile
    content <- IO.readFile inFile
    IO.writeFile outFile
        $ convertToTex template
        $ T.lines content
parseArgs _ = die "Not enough arguments specified"

main :: IO ()
main = do
    args <- getArgs
    parseArgs args
