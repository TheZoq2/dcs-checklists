#import "./util/checklist.typ": *
#import "@preview/fletcher:0.3.0"

#set page(width: 5.12in, height: 6.826in, margin: 0.12in)

#show heading: it => (
  underline(it)
)

#columns(2, [
  = Procedures

  == Startup

  #from_dsl(
    "
      Parking brake               | Set
      Battery                     | On
      Audio warning               | On
      Emergency hydraulic pump    | On
      Ignition/ventilation        | Left or Right
      Fuel shut-off               | Open
      L+R Low-Pressure fuel pumps | On
      Starter fuel pump           | On
      >verify Throttle            | Minimum
      Ignition switch             | Depressed
      >event Engine RPM > 10%    |
      Throttle                    | Idle
      >event Engine RPM idle      |
    "
  )

  === INS alignment

  #from_dsl(
    "
      INS mode                    | Standby
      INS Operation               | [N]ormal
      Spare WP                    | Selected
      Initial position            | Set
      Altitude                    | Set
      >note Left [ft] - Right [m] |
      INS Mode                    | ALN
      >verify Validate light      | On
      Validate                    | Depressed
      >verify ALN caution         | On
      >event RDY(PRET) blinking   | Class 4
      >event RDY(PRET) solid      | ALGN done
      >event when aligned | 
      INS mode | Normal
    "
  )

  === Startup cont.


  #from_dsl(
    "
      Radar                         | Warming up
      HUD                           | On
      HDD                           | On
      Radar altimeter               | On
      HUD Altimeter                 | H
      HSI mode                      | NAV
      EW Mode                       | Standby
      Jammer                        | On
      RWR                           | On
      MLWS                          | On
      Flare mode                    | SA/Auto
      Autopilot system              | Tested
      Fly by wire                   | Tested
      Pitot heat                    | On
      Aux ADI Power                 | Energized
      ADI                           | Uncaged
      IFF                           | SECT or FULL
      Weapons                       | Loaded
      NVGs                          | As desired
      Fuel amount and bingo         | Set
      Radios                        | On
      Canopy                        | Closed and sealed
      >event INS alignment complete |
      INS Mode                      | Normal
      >important Built-in tests | Not performed
      >important Things | Recompiled
    "
  )

  === Fence in
  #from_dsl(
    "
      Lights | Set
      Master arm | On
      Gun arm | On
      Radar | On
    "
  )

  #colbreak()

  = HOTAS bindings

  == Normal (RWS)

  #from_dsl("
    Undesignate | WSC depress
    Undes. magic/ nav upd. | Index Aft
    STT/TWS toggle | Index Fwd
    IFF Interrogate | Index Depress
    AP Override | Pinky
    AP Disconnect | Paddle
  ")

  == Dogfight

  Dogfight mode is controlled by the weapon system command (WSC):

  #set text(size: 9pt)
  #fletcher.diagram(
    node-stroke: black + 1pt,
    node-outset: 1pt,
    node-fill: blue.lighten(30%),
    {
    let (vertscan) = (0, 5)
    let (bore) = (0, 4)
    let (rws) = (0, 3)
    let (bah) = (0, 2)
    let (ba2) = (0, 1)

    fletcher.node(rws, [RWS], fill: green)
    fletcher.node(bore, [Boresight])
    fletcher.node(vertscan, [Vertical\Scan])
    fletcher.node(bah, [BAH \ Hor. scan])
    fletcher.node(ba2, [BA2 \ Hor.scan 2])

    fletcher.edge(rws,      bore,     "->", label: "fwd",     label-side: left)
    fletcher.edge(bore,     vertscan, "->", label: "fwd",     label-side: left)
    fletcher.edge(rws,      bah,      "->", label: "aft",     label-side: right)
    fletcher.edge(bah,      ba2,      "->", label: "aft",     label-side: right)
    fletcher.edge(vertscan, rws,      "->", label: "depress", label-side: left,  bend: 50deg)
    fletcher.edge(bore,     rws,      "->", label: none,      label-side: right, bend: 50deg)
    fletcher.edge(bah,      rws,      "->", label: none,      label-side: left,  bend: -50deg)
    fletcher.edge(ba2,      rws,      "->", label: "depress", label-side: right, bend: -50deg)
  })

])

