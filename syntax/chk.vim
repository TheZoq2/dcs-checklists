if exists('b:current_syntax') | finish | endif

syn region ChkHeading1 start="##\@!" end="#*\s*$" keepend oneline
syn region ChkHeading2 start="###\@!" end="#*\s*$" keepend oneline

syntax match ChkEvent ">event"
syntax match ChkWeapon ">weapon"
syntax match ChkNote ">note"
syntax match ChkWarning ">warn"
syntax match ChkSub ">sub"

syntax match ChkNewpage '\\newpage'


hi def link ChkHeading1 markdownH1
hi def link ChkHeading2 markdownH2
hi def link ChkEvent    NonText
hi def link ChkWeapon   NonText
hi def link ChkNote     NonText
hi def link ChkWarning  NonText
hi def link ChkSub      NonText
hi def link ChkNewpage  Keyword

let b:current_syntax = 'chk'
