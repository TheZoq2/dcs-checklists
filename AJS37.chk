# Procedures
## Mission planning
Target      | Known
Ingress     | Planned
Egress      | Planned
Target QFE  | Calculated
Takeoff QFE | Calculated

## Startup

Main power           | On
LT-KRAN              | On
Throttle             | Ground idle
Generator            | On
START                | Hold 2 sec
Oxygen               | On
Rearm   | Done
>event Engine started |
Load mission plan    | 9099
Landing airfield     | Set
>sub Weapon setup    |
Altimeter            | Set for target
Data seletor         | Akt, Output
Takeoff waypoint     | Set
Master mode          | Nav
Parking brake        | Disengaged
>note Ready to taxi  |



## Pre-takeoff
Runway        | Clear
ATC           | Notified
Canopy        | Closed
Ejection seat | Armed
Pitch trim    | 3 deg
AFK           | Off

## After-takeoff
Gear up  | Check
Waypoint | Set

## Landing
Master mode     | Landing
AFK             | Engaged
AFK AOA         | As desired
Thrust reverser | Engaged
>event On final  |
Landing gear    | Deployed

## After landing
Thrust reverser | Disengaged
AFK             | Disengaged





\newpage

# M/71 (High Drag)

## Planning
Target QFE                  | Calculated

## Weapon Setup
Target Waypoint             | M*
>note Takt \then 9 \then B* |
Weapon selector             | BOMB PLAN
Impact interval             | Set

## Execution
>event Before WP                 |
HUD                             | Lower
SLAV SI                         | Off
>note Approaching target        |
Altitude                        | 100-150m
>event Weapon arm line           | Visible
Trigger                         | Unsafe
>event Tgt in drop points |
>weapon Pickle                  |
Evade 5G                        |
Trigger                         | Safe
Master mode                     | Nav


\newpage

# M/71 Low Drag (DYK)

## Planning
Target QFE                        | Calculated

## Weapon setup
Target Waypoint                   | M*
>note Takt \then 9 \then B*       |
Pop up point                      | Set
>note Takt\then HDGDDD\then Bx    |
>note Heading \textbf{to} target  |
>note Distance in $\frac{km}{10}$ |
Verify pop up point               | Check
Weapon selector | Bomb DYK
Interval        | As desired

## Execution
>note Before WP                 |
HUD                             | Lower
SLAV SI                         | Off
>event Switch from B* to M*        |
Master mode                     | Anf
>note At pop up point           |
Pull up, rotate on target       | Done
>event Tgt aligned               |
Trigger                         | Unsafe
>event Firing queue flashing     |
>weapon Pickle                  |
Evade 4G                        |
Trigger                         | Safe
Master mode                     | Nav

\newpage
# ARAK M/70B Rockets (Short Range)

## Planning
Target QFE                        | Calculated

## Weapon setup
Target Waypoint                   | M*
>note Takt \then 9 \then B*       |
Pop up point                      | Set
>note Takt\then HDGDDD\then Bx    |
>note Heading \textbf{to} target  |
>note Distance in $\frac{km}{10}$ |
Verify pop up point               | Check
Weapon selector     | Attack
Release mode    | Serie

## Execution

>event Before WP       |
HUD                    | Low
Slav SI                | Off
>event Crossing B*     |
Master mode            | ANF
>event Pop up point    |
Pull up and rotate     |
>event Aligned         |
>warn Trigger          | Unsafe
>event Range indicator | Flashing
>weapon Fire           |
Pull away              | 5G
Master mode            | Nav


\newpage
# blank

\newpage

# RB-15F Anti-Ship

## Preparation
Target QFE         | Checked
Waypoint on target | M*

## Weapon setup

Weapon selector            | Attack
Release mode               | As desired
Targetimg mode             | As desired
>note STD preset profile   |
>note VALB pilot selection |
RB mode                    | As desired
>sub TAKT \then 80000x     |
Data selector              | AKT, Out


## Execution

Radar mode                       | A1
Radar range                      | As desired
>event Fly towards release point |
Bx8, target point                | Created
Bx6, descent point               | Created
Bx7, course change point         | Created
Bx9, self destruct point         | Created
>note See Setting Markpoints     |
Release altitude                 | 50-2000m
>event Target line flashing |
>weapon Fire                     |

## Setting Markpoints
Bx*: Bx\then *\then T1     |
Move mark \then TV confirm |
TV: Confirm                |




## Target mode Modes
Multi target, Medium area | 800002

